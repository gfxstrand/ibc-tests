#! /bin/bash

SRC_PATH="$1"
FILTER_PATH="$2"

for file in `(cd $SRC_PATH && find *) | grep 'shader_test$'`; do
    if ./run "$SRC_PATH/$file" > /dev/null 2> /dev/null; then
        echo "PASSED: $file"
        mkdir -p `dirname "$FILTER_PATH/$file"`
        ln -s -r "$SRC_PATH/$file" "$FILTER_PATH/$file"
    else
        echo "CRASHED: $file"
    fi
done
