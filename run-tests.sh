#! /bin/bash

VK_RUNNER=../src/vkrunner

PASS=0
FAIL=0

for file in *.shader_test; do
    if ${VK_RUNNER} $file 2> /dev/null | grep -q pass; then
        PASS=`expr $PASS + 1`
        echo "$file: pass"
    else
        FAIL=`expr $FAIL + 1`
        echo "$file: fail"
    fi
done

echo
echo "Passed: $PASS"
echo "FAILED: $FAIL"

test $FAIL -eq 0
